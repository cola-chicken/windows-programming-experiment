﻿using System;

namespace StringTest
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] nums1 = { 1, 7, 3, 6, 5, 6 };
            PoivtIndex poivtIndex = new PoivtIndex();
            Console.WriteLine(poivtIndex.PivotIndex(nums1));

            int[] nums = { 1, 3, 5, 6 };
            int target = 5;
            SearchIndex searchIndex = new SearchIndex();
            Console.WriteLine(searchIndex.SearchInsert(nums, target));

            char[] s = { 'h', 'e', 'l', '1', 'o' };
            ReverseString reverseString = new ReverseString();
            reverseString.ReverseStr(s);
            Console.WriteLine(s);

        }

    }

// 寻找数组中心下标
    class PoivtIndex {
        public int PivotIndex(int[] nums)
        {
            int sum = 0;
            int total = 0;
            for (int i = 0; i < nums.Length; i++)
            {
                total += nums[i];
            }
            for (int i = 0; i < nums.Length; i++)
            {
                sum += nums[i];
                if (sum == total - sum + nums[i])
                {
                    return i;
                }
            }
            return -1;
        }
    }

    //寻找插入元素
    class SearchIndex
    {
        public int SearchInsert(int[] nums, int target)
        {
            for (int i = 0; i < nums.Length; i++)
            {
                if (target <= nums[i])
                {
                    return i;
                }
            }
            return nums.Length;
        }
    }

    ////翻转字符串
    class ReverseString
    {
        public void ReverseStr(char[] s)
        {
            int low = 0;
            int height = s.Length - 1;
            char temp;
            while (low <= height)
            {
                temp = s[low];
                s[low] = s[height];
                s[height] = temp;
                low++;
                height--;
            }
        }
    }
}
